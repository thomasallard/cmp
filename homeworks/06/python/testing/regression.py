#!/usr/bin/python

#  ================================================================================
#  ||                                                                            ||
#  ||              regression.py                                                 ||
#  ||              -------------------------------------------                   ||
#  ||              R E G R E S S I O N   T E S T   S Y S T E M                   ||
#  ||                                                                            ||
#  ||              D E M O N S T R A T I O N   C O D E                           ||
#  ||              -------------------------------------------                   ||
#  ||                                                                            ||
#  ||       Developed by: Scott R. Runnels, Ph.D.                                ||
#  ||                     University of Colorado Boulder                         ||
#  ||                                                                            ||
#  ||                For: CU-Boulder CVEN 5838-500 and NCEN 5838-570             ||
#  ||                                                                            ||
#  ||           Copyright 2020 Scott Runnels                                     ||
#  ||                                                                            ||
#  ||                     Not for distribution or use outside of the             ||
#  ||                     this course.                                           ||
#  ||                                                                            ||
#  ================================================================================

import os
import sys
import getopt
import difflib

def fatalError(msg):

    f = open('FatalError.txt','w')
    #print >> f , "Fatal Error: " + msg
    print("Fatal Error: " + msg, f)
    f.close()

    os.system('cat FatalError.txt')
    exit(0)

    
def regression(argv):

#    try:
#        opts, args = getopt.getopt(argv,"t: s:")
#    except:
#       fatalError('Error in command-line arguments')
#
#    for opt, arg in opts:
#        if opt == '-t':
#            test_file = arg
#
#        if opt == '-s':
#            std_file = arg


    try:
        CODE = os.environ['CODE']
    except:
        fatalError('You must first set your CODE environment variable \n')

    workingDir = os.getcwd()

    tests = ['test_01' , 'test_02' , 'test_03', 'test_mms_01' , 'test_mms_03','test_mms_04', 'unitTest_Eval_D', 'test_converge_sine']
    #tests = ['test_01' , 'test_02' , 'test_03', 'test_mms_01' , 'test_mms_03','test_mms_04', 'unitTest_Eval_D']


    os.system('rm -f FatalError.txt')

    g = open('../../doc/testReport/testCases.tex','w')
    h = open('testResultsSummary.txt','w')
    # print >> h
    # print >> h , "========================"
    # print >> h , "Regression Test Summary "
    # print >> h , "========================"
    # print >> h
    
    print(' \n', h)
    print("======================== \n", h)
    print("Regression Test Summary  \n", h)
    print("======================== \n", h)
    print(' \n', h)

    for t in tests:
        print("Running test " + t + "   \n")
        os.chdir('cases/' + t )
        os.system('./compare.py')
        os.system('cat testResults.txt')

        # Before leaving this test directory, copy the
        # documentation to the testReport area

        targetDir = CODE + '/doc/testReport/tests/' + t

#        if not os.isdir(targetDir):
        os.system('mkdir -p        ' + CODE + '/doc/testReport/tests/' + t )
            
        os.system('cp    latex.tex ' + CODE + '/doc/testReport/tests/' + t )
        os.system('cp    *.txt     ' + CODE + '/doc/testReport/tests/' + t )
        os.system('cp    *.png     ' + CODE + '/doc/testReport/tests/' + t )

        # print >> g , '\\clearpage'
        # print >> g , '\\input{tests/' + t + '/latex.tex}'
        # print >> g , '\\vspace{1cm}'
        # print >> g , '\\noindent \\verb|Note: This test case is in directory ' + t + '|'
        
        print('\\clearpage \n', g)
        print('\\input{tests/' + t + '/latex.tex} \n', g)
        print('\\vspace{1cm} \n', g)
        print('\\noindent \\verb|Note: This test case is in directory ' + t + '| \n', g)

        # Append results to summary file

        with open('testResults.txt','r') as resultFile:
            result = resultFile.readlines()
            #print >> h, t + '       ' + result[0].replace('\n','')
            print(t + '       ' + result[0].replace('\n','')+' \n', h)
        resultFile.close()
        
        # Finished, go back to  original directory
        
        os.chdir(workingDir)


    g.close()
    h.close()

    os.system('cat testResultsSummary.txt')

if __name__ == "__main__":

    regression(sys.argv[1:])

    
