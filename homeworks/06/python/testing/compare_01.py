#!/usr/bin/python
#  ================================================================================
#  ||                                                                            ||
#  ||              compare_01.py                                                 ||
#  ||              -------------------------------------------                   ||
#  ||              R E G R E S S I O N   T E S T   S Y S T E M                   ||
#  ||                                                                            ||
#  ||              D E M O N S T R A T I O N   C O D E                           ||
#  ||              -------------------------------------------                   ||
#  ||                                                                            ||
#  ||       Developed by: Scott R. Runnels, Ph.D.                                ||
#  ||                     University of Colorado Boulder                         ||
#  ||                                                                            ||
#  ||                For: CU-Boulder CVEN 5838-500 and NCEN 5838-570             ||
#  ||                                                                            ||
#  ||           Copyright 2020 Scott Runnels                                     ||
#  ||                                                                            ||
#  ||                     Not for distribution or use outside of the             ||
#  ||                     this course.                                           ||
#  ||                                                                            ||
#  ================================================================================

import os
import sys
import getopt
import difflib

def fatalError(msg):

    f = open('FatalError.txt','w')
    print >> f , "Fatal Error: " + msg
    f.close()
    exit(0)

    
def compare(argv):

    try:
        opts, args = getopt.getopt(argv,"t: s:")
    except:
        fatalError('Error in command-line arguments')

    for opt, arg in opts:
        if opt == '-t':
            test_file = arg

        if opt == '-s':
            std_file = arg

#    print "Comparing " + test_file + " to standard file " + std_file 

    # Default is that we fail the test
    
    os.system("rm -f testResults.txt")
    
    result = 'FAILED'

    # Open the two files to be compared

    try:
        with open(test_file) as f1:
            f1_text = f1.read()
        with open(std_file) as f2:
            f2_text = f2.read()
    except:
        f1_text=['NULL0']
        f2_text=['NULL1']
        print "Error reading one or both of the files to be compared."

    # Compare the two files

    try:
        diffList = []
        for line in difflib.unified_diff( f1_text , f2_text ):
            diffList.append(line)
    except:
        print "Cannot find one or more of the comparison files"
        

    # Report results

    g = open("testResults.txt","w")

    if len(diffList) == 0:
        print >> g , 'passed'
    else:
        print >> g , 'FAILED'

    g.close()

    

if __name__ == "__main__":

    compare(sys.argv[1:])

    
