//  ================================================================================
//  ||                                                                            ||
//  ||              fd                                                            ||
//  ||              -------------------------------------------                   ||
//  ||              F I N I T E   D I F F E R E N C E                             ||
//  ||                                                                            ||
//  ||              D E M O N S T R A T I O N   C O D E                           ||
//  ||              -------------------------------------------                   ||
//  ||                                                                            ||
//  ||       Developed by: Scott R. Runnels, Ph.D.                                ||
//  ||                     University of Colorado Boulder                         ||
//  ||                                                                            ||
//  ||                For: CU-Boulder CVEN 5838-500 and NCEN 5838-570             ||
//  ||                                                                            ||
//  ||           Copyright 2020 Scott Runnels                                     ||
//  ||                                                                            ||
//  ||                     Not for distribution or use outside of the             ||
//  ||                     this course.                                           ||
//  ||                                                                            ||
//  ================================================================================

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>
#include "stdio.h"
#include "math.h"
using std :: string;
using std :: vector;
using std :: stringstream;
using std :: cout;
using std :: endl;
using std :: ifstream;

#define rLOOPf for ( int r = 1 ; r <= nrowsf ; ++r )
#define rLOOP  for ( int r = 1 ; r <= nrows  ; ++r )
#define cLOOPf for ( int c = 1 ; c <= nrowsf ; ++c )
#define cLOOP  for ( int c = 1 ; c <= nrows  ; ++c )
#define iLOOP  for ( int i = 1 ; i <= nx     ; ++i )
#define iLOOPi for ( int i = 2 ; i <= nx-1   ; ++i )
#define jLOOP  for ( int j = 1 ; j <= ny     ; ++j )
#define jLOOPi for ( int j = 2 ; j <= ny-1   ; ++j )

#define pLOOP  for ( int p = 1 ; p < npts*npts ; ++p )

#define VI  vector<int>
#define VD  vector<double>
#define VS  vector<string>
#define VDD vector<vector<double> >
