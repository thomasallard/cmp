//  ================================================================================
//  ||                                                                            ||
//  ||              fd                                                            ||
//  ||              -------------------------------------------                   ||
//  ||              F I N I T E   D I F F E R E N C E                             ||
//  ||                                                                            ||
//  ||              D E M O N S T R A T I O N   C O D E                           ||
//  ||              -------------------------------------------                   ||
//  ||                                                                            ||
//  ||       Developed by: Scott R. Runnels, Ph.D.                                ||
//  ||                     University of Colorado Boulder                         ||
//  ||                                                                            ||
//  ||                For: CU-Boulder CVEN 5838-500 and NCEN 5838-570             ||
//  ||                                                                            ||
//  ||           Copyright 2020 Scott Runnels                                     ||
//  ||                                                                            ||
//  ||                     Not for distribution or use outside of the             ||
//  ||                     this course.                                           ||
//  ||                                                                            ||
//  ================================================================================

#include "fd.h"
#include "TestData.h"
#include "UserInput.h"

//  ==
//  ||
//  ||      C L A S S:   L A P L A C I A N O N G R I D 
//  ||
//  ||      Does the following:
//  ||
//  ||      (1) Forms a linear system Ax=b representing a 2D
//  ||          finite difference approximation to Laplaces
//  ||          equation on a square.
//  ||      (2) Performs Gauss-Seidel iteration on that linear
//  ||          system up to a specified number of iterations
//  ||          or a specified tolerance.
//  ||          
//  ||       Here is what the grid looks like:
//  ||          
//  ||          
//  ||                                                     ny*nx
//  ||       *-------*-------*-------*-------*  ...   -------*
//  ||       |       |       |       |       |               |
//  ||       |       |       |       |       |               |
//  ||       .       .       .       .       .               .
//  ||       .       .       .       .       .               .
//  ||       .       .       .       .       .               .
//  || 2nx+1  
//  ||       *-------*-------*-------*-------*  ...   -------*
//  ||       |       |       |       |       |               |
//  ||       |       |       |       |       |               |
//  ||       |       |       |       |       |               |
//  ||  nx+1 |       |       |       |       |          2nx  |
//  ||       *-------*-------*-------*-------*  ...   -------*
//  ||       |       |       |       |       |               |
//  ||       |       |       |       |       |               |
//  ||       |       |       |       |       |               |
//  ||       |       |       |       |       |               |
//  ||       *-------*-------*-------*-------*  ...   -------*
//  ||      1       2       3       4                       nx  
//  ||    
//  == 

class LaplacianOnGrid
{

public:

  int nx    , ny   , nrows;
  double length, dx;
  VDD A ;
  VD  b ;
  VDD D ; 
  VDD C ; 
  VD  phi ;       // New time-step solution
  VD  phi_old ;   // Previous time-step solution
  VD  phi_anl ;   // Analytical solution for verification purposes, typically MMS
  VD  residual ;  // Residual b - A*phi_approx on this grid
  VD  error ;     // Error    x_exact - x_approx on this grid
  string Name;
  string mmsName;
  VDD IFC, ICF;   // Matrices (useed by the coarse grid only) for Retriction & Prolongation
  

  //  ==
  //  ||
  //  ||  Constructor:  Initialize values
  //  ||
  //  ==

  LaplacianOnGrid(int ncell_x, int ncell_y, string _Name , string _mmsName)
  {
    nx     = ncell_x + 1;
    ny     = ncell_y + 1;
    nrows  = nx*ny;
    length = 1.;
    dx     = length / (nx-1);
    Name   = _Name;
    mmsName = _mmsName;

    A.resize(nrows+1); rLOOP A[r].resize(nrows+1);
    D.resize(nrows+1); rLOOP D[r].resize(nrows+1);
    C.resize(nrows+1); rLOOP C[r].resize(nrows+1);
    b.resize(nrows+1);
    phi.resize(nrows+1);      phi_old.resize(nrows+1);  phi_anl.resize(nrows+1); 
    residual.resize(nrows+1); error.resize(nrows+1);

    rLOOP phi_old[r] = 5.;  // Initial conditions...hard-coded here
    
  }

  //  ==
  //  ||
  //  ||  Form Linear System Ax = b
  //  ||
  //  ==

  void FormLS(UserInput &UI)
  {
    
    // ---------------------------------------------------
    // Initialize linear system
    // ---------------------------------------------------
    
    rLOOP cLOOP A[r][c] = 0.;
    rLOOP cLOOP D[r][c] = 0.;
    rLOOP cLOOP C[r][c] = 0.;
    rLOOP b[r] = 0.;
    rLOOP phi[r] = 0.;
    
    // ---------------------------------------------------
    // Form matrix entries for the interior grid points
    // ---------------------------------------------------

    double dx2 = dx*dx;
    
    iLOOPi 
      jLOOPi
      {
	int p = pid(i,j);
	double Dval = EvalD(UI , phi[ pid(i,j) ] , false );
	D[ p ][ p    ] = -Dval * 4./dx2;
	D[ p ][ p+1  ] =  Dval * 1./dx2;
	D[ p ][ p-1  ] =  Dval * 1./dx2;
	D[ p ][ p+nx ] =  Dval * 1./dx2;
	D[ p ][ p-nx ] =  Dval * 1./dx2;
      }
      
    // ---------------------------------------------------
    // Form C matrix (transient term)
    //
    // The max (0. , UI.C) is a band-aid for a bad
    // design decision wherein the user can opt for a
    // steady state solution by setting the transient
    // coefficient to a negative number.
    //
    // ---------------------------------------------------

    rLOOP C[r][r] = std::max( 0. , UI.C );

    // ---------------------------------------------------
    // Form Complete system
    // ---------------------------------------------------

    // Matrix:
    
    rLOOP cLOOP A[r][c] = C[r][c] - UI.dt * D[r][c];

    // RHS:

    rLOOP b[r] = C[r][r] * phi_old[r];
      
    // ---------------------------------------------------
    // Add, to RHS, the MMS forcing function
    // ---------------------------------------------------

    iLOOP jLOOP
      {
       b[ pid(i,j) ] += Lphi_MMS( (i-1)*dx , (j-1)*dx );
      }
    
    // ---------------------------------------------------
    // Apply boundary conditions
    // ---------------------------------------------------

    iLOOP
      {
	int p,j;
	p = pid(i, 1);  A[ p ] [ p ] = 1. ;  b[ p ] =  1.;
	p = pid(i,ny);  A[ p ] [ p ] = 1. ;  b[ p ] = -1.;
      }
    jLOOP
      {
	int p,i;
	p = pid(1, j);  A[ p ] [ p ] = 1. ;  b[ p ] =  1.;
	p = pid(nx,j);  A[ p ] [ p ] = 1. ;  b[ p ] = -1.;
      }

    // Adjust BCs if MMS is used
   
    if ( mmsName != "none" )
      {
	iLOOP
	  {
	    int p,j;
	    j = 1;  p = pid(i,j);  A[ p ] [ p ] = 1. ;  b[ p ] = phi_MMS( (i-1)*dx , (j-1)*dx );
	    j = ny; p = pid(i,j);  A[ p ] [ p ] = 1. ;  b[ p ] = phi_MMS( (i-1)*dx , (j-1)*dx );
	  }
	jLOOP
	  {
	    int p,i;
	    i = 1;  p = pid(i,j);  A[ p ] [ p ] = 1. ;  b[ p ] =  phi_MMS( (i-1)*dx , (j-1)*dx );
	    i = nx; p = pid(i,j);  A[ p ] [ p ] = 1. ;  b[ p ] =  phi_MMS( (i-1)*dx , (j-1)*dx );
	  }
      }

    // ---------------------------------------------------
    // Populate analytical solution
    // ---------------------------------------------------

    if ( mmsName != "none" ) iLOOP jLOOP phi_anl [ pid(i,j) ] =  phi_MMS( (i-1)*dx , (j-1)*dx ); 

  }

  //  ==
  //  ||
  //  ||  Material model evaluation
  //  ||
  //  ==

  #include "TestHarness_EvalD.h"
  
  double EvalD( UserInput &UI , double phiVal , bool Testing )
  {
    if ( UI.TestMe("EvalD") )  if ( !Testing ) TestHarness_EvalD( UI );
    
    return ( UI.D0 + UI.D1*phiVal + UI.D2*phiVal*phiVal );
  }


  //  ==
  //  ||
  //  ||  MMS routines
  //  ||
  //  ==

  double phi_MMS( double x , double y )
  {
    if ( mmsName == "LinearIn_xy")  return phi_MMS_LinearIn_xy(x,y);
    if ( mmsName == "QuadIn_xy"  )  return phi_MMS_QuadIn_xy(x,y);
    if ( mmsName == "SineIn_xy"  )  return phi_MMS_SineIn_xy(x,y);
    return 0.;
  }


  double Lphi_MMS( double x , double y )
  {
    if ( mmsName == "LinearIn_xy")  return Lphi_MMS_LinearIn_xy(x,y);
    if ( mmsName == "QuadIn_xy"  )  return Lphi_MMS_QuadIn_xy(x,y);
    if ( mmsName == "SineIn_xy"  )  return Lphi_MMS_SineIn_xy(x,y);
    return 0.;
  }


  // Manufactured Solutions
  
  double phi_MMS_LinearIn_xy(double x, double y)  { return (   x +   y);  }
  double phi_MMS_QuadIn_xy  (double x, double y)  { return ( x*x + y*y);  }
  double phi_MMS_SineIn_xy  (double x, double y)  { return   sin(3.*x)    ;  }

  // Forcing functions for the Manufactured Solutions (steady state verification only)
  
  double Lphi_MMS_LinearIn_xy(double x, double y)  { return 0.; }
  double Lphi_MMS_QuadIn_xy(double x, double y)    { return -4.; }
  double Lphi_MMS_SineIn_xy(double x, double y)    { return  9.*sin(3.*x); }
  
  //  ==
  //  ||
  //  ||  Utility routines
  //  ||
  //  ==

  int pid(int i,int j) { return i + (j-1)*nx; }  // Given i-j, return point ID.
  void UpdatePhi(){ rLOOP phi_old[r] = phi[r]; }


  #include "plotter.h"
  #include "gauss_seidel.h"
  #include "gmg.h"

  //  ==
  //  ||
  //  ||  Error norms
  //  ||
  //  ==

  double RMS_error()
  {
    double sum = 0;   int nval = 0;
    
    iLOOP jLOOP
      {
       double error = phi[ pid(i,j) ] - phi_MMS ( (i-1)*dx , (j-1)*dx );
       sum += error*error;
       ++nval;
      }
    return sqrt(sum)/nval;
  }

  double L2_error()  // Integral-based error
  {
    double dA       = (dx/10.) * (dx/10.); // Divide each cell into 100 little cells
    double area     = 0.;
    double integral = 0.;

    iLOOP jLOOP
      {
       // Rectangle rule integration...10 cells by 10 cells inside each cell

       for ( int K = 1 ; K <= 10 ; ++ K )
       for ( int L = 1 ; L <= 10 ; ++ L )
          {
           double error = phi [ pid(i,j) ] - phi_MMS ( (i-1)*dx , (j-1)*dx ) ;
           integral += error * error * dA;
           area     +=                 dA;
          }
      }
    return sqrt(integral)/area;
  }

  double Max_error()
  {
    double error = 0.;
    iLOOP jLOOP
      {
       double ePoint = phi[ pid(i,j) ] - phi_MMS ( (i-1)*dx , (j-1)*dx );
       ePoint = fabs(ePoint);
       if ( ePoint > error ) error = ePoint;
      }
    return error;
  }

};



//  ==
//  ||
//  ||
//  ||  Main Program
//  ||
//  ||
//  ==

int main()
{

  UserInput UI;


  cout << "\n";
  cout << "---------------------------------------------\n";
  cout << "|                                           |\n";
  cout << "| F I N I T E   D I F F D E R E N C E       |\n";
  cout << "| D E M O   C O D E                         |\n";
  cout << "|                                           |\n";
  cout << "---------------------------------------------\n";
  cout << "\n";

  UI.ReadInput();
  UI.EchoInput();

  // If the transient term is negative, the user is saying
  // they only want to solve D nabla^2 phi = 0.
  
    if ( UI.C <= 0. )
      {
      }



    
    if ( UI.physics == "steadyState")
    {
      LaplacianOnGrid F(UI.ncell_x,UI.ncell_y, "F" , UI.mmsName);

      for ( int SAiter = 0 ; SAiter < UI.SA_maxIt ; ++SAiter )
	{
	  cout << endl;
	  cout << "Successive Approximation Iteration " << SAiter << endl;
	  F.FormLS(UI);
	  F.GaussSeidel(200, F.b , F.phi );
	}
      F.plot("phi",F.phi,0);
      F.plot("phi_anl",F.phi_anl,0);

      cout << "RMS " << F.nx << " " << F.RMS_error() << endl;
      cout << "L2  " << F.nx << " " << F.L2_error()  << endl;
      cout << "Max " << F.nx << " " << F.Max_error() << endl;
    }


    
    if ( UI.physics == "steadyState" && UI.multigrid )
    {
      LaplacianOnGrid F(UI.ncell_x   , UI.ncell_y   , "F_gmg" ,  UI.mmsName);
      LaplacianOnGrid C(UI.ncell_x/2 , UI.ncell_y/2 , "C_gmg" ,  UI.mmsName);

      F.FormLS(UI);
      C.FormLS(UI);

      C.FormICF(F);
      C.FormIFC(F);
      
      F.GaussSeidel(10, F.b , F.phi );                    // Perform a few iterations on F, solving A*phi = b
      F.ComputeResidual();                               // Compute residual on F, r = b - A*phi
      C.MatRestrict(        C.residual , F, F.residual );  // Map F's residual, r,  onto C's grid
      C.GaussSeidel(50, C.residual , C.error        );  // Solve for error, e, A*e = r on C
      C.MatProlong (        C.error    , F, F.error  );  // Map C's error, e,  onto F's grid
      F.Correct();                                       // Use F's new estimate for error, e, to  update phi.
      F.GaussSeidel(200, F.b , F.phi );                  // Complete the solve step
      F.plot("phi",F.phi,0); 
      exit(0);
    }

  // Transient Solve

  
    if ( UI.physics == "transient")
      {
	int time_index = 0;                             
	LaplacianOnGrid F(UI.ncell_x,UI.ncell_y, "F" ,UI.mmsName); 
	for ( double time = 0.; time <= UI.t_final ; time += UI.dt )
	  {
	    F.FormLS(UI);
	    F.GaussSeidel(200, F.b , F.phi);
	    cout << "Time = " << time << endl;
	    F.UpdatePhi();   
	    F.plot("phi",F.phi,time_index); 
	    ++time_index;   
	  }
      }
  

}



