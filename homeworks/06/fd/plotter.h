//  ================================================================================
//  ||                                                                            ||
//  ||              fd                                                            ||
//  ||              -------------------------------------------                   ||
//  ||              F I N I T E   D I F F E R E N C E                             ||
//  ||                                                                            ||
//  ||              D E M O N S T R A T I O N   C O D E                           ||
//  ||              -------------------------------------------                   ||
//  ||                                                                            ||
//  ||       Developed by: Scott R. Runnels, Ph.D.                                ||
//  ||                     University of Colorado Boulder                         ||
//  ||                                                                            ||
//  ||                For: CU-Boulder CVEN 5838-500 and NCEN 5838-570             ||
//  ||                                                                            ||
//  ||           Copyright 2020 Scott Runnels                                     ||
//  ||                                                                            ||
//  ||                     Not for distribution or use outside of the             ||
//  ||                     this course.                                           ||
//  ||                                                                            ||
//  ================================================================================

void plot(string descriptor, VD &field, int time_index)   
  {
    stringstream time_index_str; time_index_str << time_index;  
				   
    string filename = Name + "_" + descriptor + "_" + time_index_str.str() + ".plt";  
    std::fstream f;
    f.open(filename.c_str(),std::ios::out);
    iLOOP 
      {
	jLOOP f << dx*(i-1) << " " << dx*(j-1) << " " << field[pid(i,j)] << endl;
	f << "\n";
      }
    f.close();
  }




void plot(string descriptor, VD &x , VD &y)
  {
    string filename = descriptor + ".plt";
    std::fstream f;
    f.open(filename.c_str(),std::ios::out);

    for ( int i = 1 ; i <= x.size() - 1 ; ++i )
      {
	f << x[i] << " " << y[i] << endl;
      }
    f.close();
  }
