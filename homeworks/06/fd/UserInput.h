//  ================================================================================
//  ||                                                                            ||
//  ||              fd                                                            ||
//  ||              -------------------------------------------                   ||
//  ||              F I N I T E   D I F F E R E N C E                             ||
//  ||                                                                            ||
//  ||              D E M O N S T R A T I O N   C O D E                           ||
//  ||              -------------------------------------------                   ||
//  ||                                                                            ||
//  ||       Developed by: Scott R. Runnels, Ph.D.                                ||
//  ||                     University of Colorado Boulder                         ||
//  ||                                                                            ||
//  ||                For: CU-Boulder CVEN 5838-500 and NCEN 5838-570             ||
//  ||                                                                            ||
//  ||           Copyright 2020 Scott Runnels                                     ||
//  ||                                                                            ||
//  ||                     Not for distribution or use outside of the             ||
//  ||                     this course.                                           ||
//  ||                                                                            ||
//  ================================================================================


class UserInput
{

 public:

  int ncell_x, ncell_y;
  
  double dt;          // Time step
  double t_final;     // Time limit for simulation
  double D0, D1, D2;  // Diffusion Coefficients, D(phi) = D0 + D1*phi + D2*phi^2
  double C;           // Transient Coefficient
  string physics;     // What type of simulation is to be performed
  bool   multigrid;   // True if geometric multigrid is to be used
  string mmsName;     // Name, if any, of the MMS solution for verification
  int    SA_maxIt;    // Maximum Successive Approximation iterations

  int  n_fnc;           // Number of routines to be tested
  VS   fnc;             // List of routine names to be tested
  vector<TestData > TD; // Array of test data objects
  

  UserInput()
    {
      ncell_x = ncell_y = 10;
      t_final = 10.;
      dt      = 1.;
      D0      = 1.;   D1 = 0. ;  D2 = 0.;
      C       = 1.;
      physics = "";
      multigrid = false;
      mmsName = "none";
      SA_maxIt = 1;
      n_fnc   = 0;
    }

  void ReadInput()
    {
      // Open a file named "input" and look for commands.  After a known command is
      // found, read what is on the line(s) that follow.

      string line ; ifstream inputFile("input");

      while ( getline(inputFile,line) )
	{
	  if ( line == "$ncell"      ) { inputFile >> ncell_x    ;  inputFile >> ncell_y;                    }
	  if ( line == "$t_final"    ) { inputFile >> t_final    ;                                           }
	  if ( line == "$dt"         ) { inputFile >> dt         ;                                           }
	  if ( line == "$coefs"      ) { inputFile >> C          ;  inputFile >> D0     ;                    }
	  if ( line == "$Dcoefs"     ) { inputFile >> D0         ;  inputFile >> D1     ;  inputFile >> D2 ; }
	  if ( line == "$physics"    ) { inputFile >> physics    ;                                           }
	  if ( line == "$MMS"        ) { inputFile >> mmsName    ;                                           }
	  if ( line == "$multigrid"  ) { multigrid = true        ;                                           }
	  if ( line == "$SA"         ) { inputFile >> SA_maxIt   ;                                           }
	  if ( line == "$TestHarness") { inputFile >> n_fnc      ;                                           }
	}

      // Go back to the beginning of the input file and read all  of the TestData inputs

      TD.resize(n_fnc+1);
      inputFile.clear(); inputFile.seekg(0);    // Rewind the input file

      int fnc_count = 0;  // Search for and read TestData input
      while (getline(inputFile,line))
	{
	  if ( line == "$TestData")
	    {
	      ++fnc_count ;
	      TD[fnc_count].ReadData(inputFile);
	    }
	}

      inputFile.close();
      
    }

  void EchoInput()
  {

    cout << "User Input Values:" << endl;
    cout << "---------------------------------" <<  endl;
    cout << endl;
    cout << "ncell_x =  " << ncell_x  << endl;
    cout << "ncell_y =  " << ncell_y  << endl;
    cout << "t_final =  " << t_final  << endl;
    cout << "dt      =  " << dt       << endl;
    cout << "D0      =  " << D0       << endl;
    cout << "D1      =  " << D1       << endl;
    cout << "D2      =  " << D2       << endl;
    cout << "C       =  " << C        << endl;
    cout << "           "             << endl;
    cout << "SA_maxIt = " << SA_maxIt << endl;
    cout << "           "             << endl;
    cout << "MMS Solution =  " << mmsName            << endl;
      

  }

  // This routine returns true if the provided "fnc_name" is
  // in the list of functions to be tested.
  
  bool TestMe( string fnc_name )
  {
    if ( n_fnc == 0 ) return false;
    for ( int t = 1 ; t <= n_fnc ; ++t )
      if ( fnc_name == TD[t].Name ) return true;

    return false;
  }
  

  // This routine returns a pointer the TestData object intended
  // for routine "fnc_name".
  
  TestData *GetTD( string fnc_name )
  {
    for ( int t = 1 ; t <= n_fnc ; ++t )
      if ( fnc_name == TD[t].Name ) return &TD[t];

    return NULL;
  }
  


};

