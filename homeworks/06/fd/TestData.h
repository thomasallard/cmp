

//
//  Purpose of this class is to provide a mechanism for getting data 
//  from the input file directly to a routine to be tested.  At the
//  same time, it provides part of the list of routines to be tested.
//


class TestData
{

 public:

  int nD   ;  VD Ddata ;  // Number and array of double-precision data
  int nI   ;  VI Idata ;  //    "    "    "    "   integer        data
  string Name;            // Name of the routine to be tested

  void ReadData(ifstream &input_file)
  {
    input_file >> Name;                     // Read the routine to be tested
    input_file >> nD ; Ddata.resize(nD+1);  // Read the number of doubles to be read
    input_file >> nI ; Idata.resize(nI+1);  // Read the number of doubles to be read

    for ( int i = 1 ; i <= nD ; ++i ) input_file >> Ddata[i];
    for ( int i = 1 ; i <= nI ; ++i ) input_file >> Idata[i];  

  }



};
