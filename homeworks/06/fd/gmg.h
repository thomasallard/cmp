
//  ==
//  ||
//  ||  Restriction and Prolongation is performed on the Coarse Grid
//  ||  LaplacianOnGrid object.  When node numbers for  the Fine Grid
//  ||  are needed by Coarse grid object, the proper "pid" function
//  ||  must be called and, also, the i-j logical indexing must be
//  ||  adjusted to represent the finer grid.  These macros return
//  ||  the Fine grid node colocated with the Coarse grid location i,j.  
//  ||  It is labeled "Here".  These macros also return the nodes
//  ||  neighboring "Here", all on the Fine grid.
//  ||
//  ==

#define Here   I.pid ( 2*i-1     , 2*j-1     ) 
#define West   I.pid ( 2*i-1 - 1 , 2*j-1     ) 
#define East   I.pid ( 2*i-1 + 1 , 2*j-1     ) 
#define North  I.pid ( 2*i-1     , 2*j-1 + 1 ) 
#define South  I.pid ( 2*i-1     , 2*j-1 - 1 ) 
#define NW     I.pid ( 2*i-1 - 1 , 2*j-1 + 1 ) 
#define NE     I.pid ( 2*i-1 + 1 , 2*j-1 + 1 ) 
#define SE     I.pid ( 2*i-1 - 1 , 2*j-1 - 1 ) 
#define SW     I.pid ( 2*i-1 + 1 , 2*j-1 - 1 ) 

//  ==
//  ||
//  ||  Form ICF Matrix for mapping coarse grid to fine grid
//  ||
//  ==

void FormICF(LaplacianOnGrid &I)  
  {
    //    Here, "I" is the fine grid
    //
    //    Field on F           ICF              Field on C
    //    +--     --+    +--              --+   +--     --+ 
    //    |   1     |    | 1 2 3 ...  nrows |   |   1     | 
    //    |   2     |    | 2                |   |   2     | 
    //    |   3     |    | 3                |   |   .     | 
    //    |         |    |                  |   |   .     | 
    //    |   .     |    | .                |   |  nrows  | 
    //    |   .     | =  | .                |   +--     --+
    //    |   .     |    | .                |
    //    |         |    |                  |
    //    |         |    |                  |
    //    |         |    |                  |
    //    | I.nrows |    | I.nrows          |
    //    +--     --+    +--              --+
    //
    
    // Mapping weights
    
    double ha = 1./2.;      double th = 1./3.; 
    double ei = 1./8.;      double sx = 1./6.; 
    double qu = 1./4.;      double tw = 1./12.;
    double st = 1./16.; 

    ei *= 4.;    qu *= 4.;    st *= 4.;    th *= 3.;    sx *= 3.;    tw *= 3.;
    
    // Allocate ICF and initialize memory; see figure, above

    ICF.resize(I.nrows+1); for ( int r = 1 ; r <= I.nrows ; ++r ) ICF[r].resize(nrows+1);

    for ( int r = 1 ; r <= I.nrows ; ++r ) for ( int c = 1 ; c <= nrows ; ++c  ) ICF[r][c] = 0.;

    // Mapping for internal nodes

    iLOOPi jLOOPi
      { int Tp = pid(i,j); 
      
	ICF[ Here  ] [ Tp ] += qu;
	ICF[ East  ] [ Tp ] += ei; ICF[ West ] [ Tp ] += ei; ICF[ South ] [ Tp ] += ei; ICF[ North ] [ Tp ] += ei;
	ICF[ SW    ] [ Tp ] += st; ICF[ SE   ] [ Tp ] += st; ICF[ NW    ] [ Tp ] += st; ICF[ NE    ] [ Tp ] += st;
      }

    // Mapping for boundary nodes

    iLOOPi
      {	int j, Tp;
	
	j = 1 ;  Tp = pid(i,j) ; ICF[ Here ] [ Tp ] += ha ; ICF[ West ] [ Tp ] += qu ; ICF[ East ] [ Tp ] += qu ;
	j = ny;  Tp = pid(i,j) ; ICF[ Here ] [ Tp ] += ha ; ICF[ West ] [ Tp ] += qu ; ICF[ East ] [ Tp ] += qu ;
      }

    jLOOPi
      { int i, Tp;

	i =  1;  Tp = pid(i,j);  ICF[ Here  ] [ Tp ]  = ha ;  ICF[ South ] [ Tp ]  += qu ;  ICF[ North ] [ Tp ]  += qu ;
	i = nx;  Tp = pid(i,j);  ICF[ Here  ] [ Tp ]  = ha ;  ICF[ South ] [ Tp ]  += qu ;  ICF[ North ] [ Tp ]  += qu ;
      }

    int i, j, Tp;
    
    i =  1  ; j =  1 ;  Tp = pid(i,j);   ICF[ Here  ] [ Tp ] = 1. ;
    i = nx  ; j =  1 ;  Tp = pid(i,j);   ICF[ Here  ] [ Tp ] = 1. ;
    i =  1  ; j = ny ;  Tp = pid(i,j);   ICF[ Here  ] [ Tp ] = 1. ;
    i = nx  ; j = ny ;  Tp = pid(i,j);   ICF[ Here  ] [ Tp ] = 1. ;
    
  }

//  ==
//  ||
//  ||  Form IFC Matrix for mapping fine grid to coarse grid
//  ||
//  ==

  void FormIFC(LaplacianOnGrid &I)   
  {
    //    Here, "I" is the fine grid
    //
    //      Field on C	            IFC                        Field on F  
    //      +--     --+      +--                          --+  +--     --+ 
    //      |   1     |      |   1  2  3    ...   I.nrows   |  |   1     | 
    //      |   2     |      |   2                          |  |   2     | 
    //      |   .     |  =   |   .                          |  |   3     | 
    //      |   .     |      |   .                          |  |         | 
    //      |  nrows  |      |  nrows                       |  |   .     | 
    //      +--     --+	     +--                          --+  |   .     | 
    //                                                         |   .     | 
    //                                                         |         | 
    //                                                         |         | 
    //                                                         |         | 
    //                                                         | I.nrows | 
    //                                                         +--     --+ 
    
    double ha = 1./2.;     double th = 1./3.; 
    double ei = 1./8.;     double sx = 1./6.; 
    double qu = 1./4.;     double tw = 1./12.;
    double st = 1./16.; 
    
    // Allocate and initialize memory; see figure, above

    IFC.resize(nrows+1); for ( int r = 1 ; r <= nrows ; ++r ) IFC[r].resize(I.nrows+1);

    for ( int r = 1 ; r <= nrows ; ++r )
      for ( int c = 1 ; c <= I.nrows ; ++c  )
	IFC[r][c] = 0.;

    // Mappings for interior nodes: For each point Tp on the coarse grid, multiple fine grids contribute values

    iLOOPi
      jLOOPi
    {
      int Tp = pid(i,j); 
      
      IFC[ Tp ] [ Here  ] += qu;
      IFC[ Tp ] [ East  ] += ei;      IFC[ Tp ] [ West  ] += ei;      IFC[ Tp ] [ South ] += ei;      IFC[ Tp ] [ North ] += ei;
      IFC[ Tp ] [ SW    ] += st;      IFC[ Tp ] [ SE    ] += st;      IFC[ Tp ] [ NW    ] += st;      IFC[ Tp ] [ NE    ] += st;
    }

    // Mapping for boundary nodes

    iLOOPi { int j, Tp;

      j =  1;  Tp = pid(i,j) ; IFC[ Tp ][ Here  ] += ha ;  IFC [ Tp ][ West  ] += qu ;  IFC[ Tp ][ East  ] += qu ;
      j = ny;  Tp = pid(i,j) ; IFC[ Tp ][ Here  ] += ha ;  IFC [ Tp ][ West  ] += qu ;  IFC[ Tp ][ East  ] += qu ;
    }

    jLOOPi { int i, Tp;

      i =  1; Tp = pid(i,j) ;  IFC[ Tp ][ Here  ] += ha ;  IFC [ Tp ][ South ] += qu ;  IFC[ Tp ][ North ] += qu ;
      i = nx; Tp = pid(i,j) ;  IFC[ Tp ][ Here  ] += ha ;  IFC [ Tp ][ South ] += qu ;  IFC[ Tp ][ North ] += qu ;
    }

    int i, j, Tp;
    
    i =  1  ; j =  1 ;  Tp = pid(i,j);   IFC[ Tp ][ Here  ]  = 1. ;
    i = nx  ; j =  1 ;  Tp = pid(i,j);   IFC[ Tp ][ Here  ]  = 1. ;
    i =  1  ; j = ny ;  Tp = pid(i,j);   IFC[ Tp ][ Here  ]  = 1. ;
    i = nx  ; j = ny ;  Tp = pid(i,j);   IFC[ Tp ][ Here  ]  = 1. ;
	  
  }


//  ==
//  ||
//  ||  Prolongation and Restriction Operations Using Matrices
//  ||
//  ||  Note that I_field is always coming in from the "I" LaplacianOnGrid object,
//  ||  which is the fine grid.
//  ||
//  ==

void MatRestrict( VD &T_field, LaplacianOnGrid I , VD &I_field )
  {
    // Map incoming fine grid I_field ---> this object's coarse grid T_field
    
    for ( int r = 1 ; r <= nrows ; ++r ) T_field[r] = 0.;
    
    for ( int r = 1 ; r <= nrows ; ++r ) 
      for ( int c = 1 ; c <= I.nrows ; ++c  )
	T_field[r] += IFC[r][c] * I_field[c];
  }

void MatProlong( VD &T_field, LaplacianOnGrid I , VD &I_field ) 
  {
    // Map this object's coarse grid T_field ---> incoming object's fine grid I_field
    
    for ( int r = 1 ; r <= I.nrows ; ++r ) I_field[r] = 0.;
    
    for ( int r = 1 ; r <= I.nrows ; ++r )
      for ( int c = 1 ; c <= nrows ; ++c  )
	I_field[r] += ICF[r][c] * T_field[c];
  }



//  ==
//  ||
//  ||  Utilities: Compute the current residual and correct current guess with approximate error
//  ||
//  ==

void ComputeResidual() { rLOOP { residual[r]  = b[r]    ; cLOOP residual[r] -= A[r][c]*phi[c];  }  }
void Correct()         { rLOOP        phi[r] += error[r];                                          }




//  ===============================================================================================
//  ||                                                                                           ||
//  ||                                                                                           ||
//  ||                 F O R   I N S T R U C T I O N A L   U S E   O N L Y                       ||
//  ||                               (not used by the code)                                      ||
//  ||                                                                                           ||
//  ||                                                                                           ||
//  ===============================================================================================




//  ==
//  ||
//  || Purpose: Demonstrate that IFC^T * A_fine * ICF = A_coarse
//  ||
//  ==

void IFC_A_ICF( LaplacianOnGrid &C ) // To be run on the fine grid
{
  //    IFC ( nrowsC x nrowsF ) *  A ( nrowsF x nrowsF )  * ICF ( nrowsF x nrowsC ) 
  // =  IFC ( nrowsC x nrowsF ) *  AT ( nrowsF x nrowsC )  <--- A"tmp"
  // =  AC  ( nrowsC x nrowsC )  

  int nrowsF = nrows;   int nrowsC = C.nrows;

  // Allocate Memory and Initialize

  VDD AC;  AC.resize( nrowsC + 1 ); for ( int r = 1 ; r <= nrowsC ; ++r ) AC[r].resize(nrowsC+1);
  VDD AT;  AT.resize( nrowsF + 1 ); for ( int r = 1 ; r <= nrowsF ; ++r ) AT[r].resize(nrowsC+1);

  // Initialize
  
  for ( int r = 1 ; r <= nrowsC ; ++r )  for ( int c = 1 ; c <= nrowsC ; ++c ) AC[r][c] = 0.;
  for ( int r = 1 ; r <= nrowsF ; ++r )  for ( int c = 1 ; c <= nrowsC ; ++c ) AT[r][c] = 0.;

  // AT = A*ICF

  for ( int r = 1 ; r <= nrowsF ; ++r )
    for ( int c = 1 ; c <= nrowsC ; ++c )
      {
	for ( int n = 1 ; n <= nrowsC ; ++n )
	  AT[r][c] += A[r][n]*C.ICF[n][c];
      }
  
  // AC = IFC*AT
  
  for ( int r = 1 ; r <= nrowsC ; ++r )
    for ( int c = 1 ; c <= nrowsC ; ++c )
      {
	for ( int n = 1 ; n <= nrowsF ; ++n )
	  AC[r][c] += C.IFC[r][n]*AT[n][c];
      }
  
  // Multiply AC by 4

  for ( int r = 1 ; r <= nrowsC ; ++r )  for ( int c = 1 ; c <= nrowsC ; ++c ) AC[r][c] *= 4.;
  
  ShowMatrix("IFC_A_ICF.txt",AC,nrowsC);
}


//  ==
//  ||
//  || Purpose: For printing matrices (see above purpose)
//  ||
//  ==

void ShowMatrix(string filename , VDD &Mat, int _nrows)
{
  std::fstream f;  f.open(filename.c_str(),std::ios::out);
  for ( int r = 1 ; r <= _nrows ; ++r )
    {
      f << "row " << r << endl;
      for ( int c = 1 ; c <= _nrows ; ++c )  if ( fabs(Mat[r][c]) > 0. ) f << " c("  << c << ") = " << Mat[r][c];
      f << endl;
    }

  f.close();
}

void ShowMatrix(string filename)
{
  std::fstream f;  f.open(filename.c_str(),std::ios::out);
  for ( int r = 1 ; r <= nrows ; ++r )
    {
      f << "row " << r << endl;
      for ( int c = 1 ; c <= nrows ; ++c )  if ( fabs(A[r][c]) > 0. ) f << " c("  << c << ") = " << A[r][c];
      f << "RHS("  << r << ") = " << b[r];
      f << endl;
    }

  f.close();
}


//  ==
//  ||
//  || Purpose: A more direct Restriction Operator, not a matrix, but operates
//  ||          directly on a field.  Using this gives  the same answer as using
//  ||          MatRestrict.
//  ||
//  ==

//  ==
//  ||
//  ||  Restrict
//  ||
//  ||  This is for a 2-D grid, but we draw the 1-D analogy here because it is
//  ||  easier.  Below, "I" is the incoming object and "T" is "this", i.e., this
//  ||  object.
//  ||
//  ||                        1       2       3       4       5       6
//  ||   This object          T-------T-------T-------T-------T-------T
//  ||                                ^
//  ||                                |
//  ||                               /|	  \
//  ||                              / |	   \
//  ||                             /  |	    \
//  ||                            /   |					\
//  ||   Incoming object      I---I---I---I---I---I---I---I---I---I---I
//  ||                        1   2   3   4   5   6   7   8   9  10  11
//  ||
//  ||
//  ||   In 2D, here are the weightings:
//  ||
//  ||
//  ||      *------------*------------*------------*
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||                                             |
//  ||      I -----------I----------- I------------*
//  ||       1/16          1/8          1/16       |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||                                             |
//  ||      I ---------- T -----------I------------*
//  ||       1/8          1/4          1/8         |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||                                             |
//  ||      I ---------- I ---------- I------------*
//  ||       1/16          1/8          1/16       
//  ||
//  ||
//  ||    And, on the boundary:
//  ||
//  ||
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||                                             |
//  ||      I ---------- I -----------I------------*
//  ||       1/12          1/6          1/12       |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||                                             |
//  ||      I ---------- T ---------- I------------*
//  ||       1/6          1/3          1/6
//  ||
//  ||
//  ==

void Restrict( VD &T_field, LaplacianOnGrid I , VD &I_field )
{
  double ei = 1./8.;    
  double qu = 1./4.;
  double st = 1./16.; 
  double th = 1./3.;
  double sx = 1./6.;
  double tw = 1./12.;
  int Tp;      // This objections point ID number
  int iC, jC;  // The incoming objects i-j location corresponding
  // to this object's i-j location, colocated physically.
  
  rLOOP T_field[r] = 0.;
  // ------------------------------
  // All interior points
  // ------------------------------
  
  iLOOPi jLOOPi
  {
    Tp = pid(i,j); 
    
    T_field[Tp]  = qu *  I_field[Here];
    T_field[Tp] += ei * (I_field[East] + I_field[West] + I_field[South] + I_field[North] );
    T_field[Tp] += st * (I_field[ SW ] + I_field[ SE ] + I_field[ NW  ] + I_field[ NE  ] );
  }
  
  // ------------------------------
  // Boundary points
  // ------------------------------
  
  iLOOP { int j;
    j = 1 ; Tp = pid(i,j);   T_field[Tp] = qu*I_field[Here] + ei*( I_field[East] + I_field[West] ) ;
    j = ny; Tp = pid(i,j);   T_field[Tp] = qu*I_field[Here] + ei*( I_field[East] + I_field[West] ) ;
  }
  
  jLOOP { int i;
    i =  1; Tp = pid(i,j);   T_field[Tp] = qu*I_field[Here] + ei*( I_field[North] + I_field[South] ); 
    i = nx; Tp = pid(i,j);   T_field[Tp] = qu*I_field[Here] + ei*( I_field[North] + I_field[South] ); 
  }
}



//  ==
//  ||
//  || Purpose: A more direct Prolongation Operator, not a matrix, but operates
//  ||          directly on a field.  Using this gives  the same answer as using
//  ||          MatRestrict.
//  ||
//  ==

//  ==
//  ||
//  ||  Prolong
//  ||
//  ||  This is for a 2-D grid, but we draw the 1-D analogy here because it is
//  ||  easier.  Below, "I" is the incoming object and "T" is "this", i.e., this
//  ||  object.
//  ||
//  ||                        1   2   3   4   5   6   7   8   9  10  11
//  ||   Incoming object      I---I---I---I---I---I---I---I---I---I---I
//  ||
//  ||                            ^   ^   ^
//  ||                            |   |   |
//  ||                             \  |  /   Here, T's point 2 contributes
//  ||                              \ | /    to I's points 2, 3, and 4.
//  ||                               \|/      
//  ||
//  ||   This object          T-------T-------T-------T-------T-------T
//  ||                        1       2       3       4       5       6
//  ||
//  ||
//  ||   The weightings for T's contributions to the (finer) I points 
//  ||   are as follows:
//  ||
//  ||      *------------*------------*------------*
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||                                             |
//  ||      I -----------I----------- I------------*
//  ||       1/16          1/8          1/16       |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||                                             |
//  ||      I ---------- T -----------I------------*
//  ||       1/8          1/4          1/8         |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||                                             |
//  ||      I ---------- I ---------- I------------*
//  ||       1/16          1/8          1/16       
//  ||
//  ||
//  ||    And, on the boundary:
//  ||
//  ||
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||                                             |
//  ||      I ---------- I -----------I------------*
//  ||       1/12          1/6          1/12       |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||      |            |            |            |
//  ||                                             |
//  ||      I ---------- T ---------- I------------*
//  ||       1/6          1/3          1/6
//  ||
//  ||
//  ==

void Prolong( VD &T_field, LaplacianOnGrid I , VD &I_field )
{
  double ha = 1./2.;    
  double ei = 1./8.;    
  double qu = 1./4.;
  double st = 1./16.; 
  double th = 1./3.;
  double sx = 1./6.;
  double tw = 1./12.;
  double Tf;      // T_field, evaluated at this object's point ID number
  int iC, jC;  // The incoming objects i-j location corresponding
  // to this object's i-j location, colocated physically.
  
  // ------------------------------
  // Initialize I_field
  // ------------------------------
  
  for ( int p = 1 ; p <= I.nrows ; ++p ) I_field[p] = 0.;
  
  // ------------------------------
  // Scale weights
  // ------------------------------
  
  ei *= 4.;
  qu *= 4.;
  st *= 4.;
  th *= 3.;
  sx *= 3.;
  tw *= 3.;
  
  // ------------------------------
  // All interior points
  // ------------------------------
  
  iLOOPi jLOOPi
  {
    Tf = T_field[pid(i,j)]; 
    
    I_field[  Here  ] += qu * Tf;
    I_field[  West  ] += ei * Tf;
    I_field[  East  ] += ei * Tf;
    I_field[  South ] += ei * Tf;
    I_field[  North ] += ei * Tf;
    I_field[  SW    ] += st * Tf;
    I_field[  SE    ] += st * Tf;
    I_field[  NE    ] += st * Tf;
    I_field[  NW    ] += st * Tf;
  }
  
  // ------------------------------
  // Boundary points
  // ------------------------------
  
  iLOOP { int j;
    
    j = 1;  Tf = T_field[pid(i,j)]; I_field[ Here ] = ha * Tf; I_field[ West ] = qu * Tf; I_field[ East ] = qu * Tf;
    j = ny; Tf = T_field[pid(i,j)]; I_field[ Here ] = ha * Tf; I_field[ West ] = qu * Tf; I_field[ East ] = qu * Tf;
  }
  
  jLOOP { int i;
    
    i = 1;  Tf = T_field[pid(i,j)]; I_field[ Here ] = ha * Tf; I_field[ South ] = qu * Tf; I_field[ North ] = qu * Tf;
    i = nx; Tf = T_field[pid(i,j)]; I_field[ Here ] = ha * Tf; I_field[ South ] = qu * Tf; I_field[ North ] = qu * Tf;
  }
}



