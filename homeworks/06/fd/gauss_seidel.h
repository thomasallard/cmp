//  ================================================================================
//  ||                                                                            ||
//  ||              fd                                                            ||
//  ||              -------------------------------------------                   ||
//  ||              F I N I T E   D I F F E R E N C E                             ||
//  ||                                                                            ||
//  ||              D E M O N S T R A T I O N   C O D E                           ||
//  ||              -------------------------------------------                   ||
//  ||                                                                            ||
//  ||       Developed by: Scott R. Runnels, Ph.D.                                ||
//  ||                     University of Colorado Boulder                         ||
//  ||                                                                            ||
//  ||                For: CU-Boulder CVEN 5838-500 and NCEN 5838-570             ||
//  ||                                                                            ||
//  ||           Copyright 2020 Scott Runnels                                     ||
//  ||                                                                            ||
//  ||                     Not for distribution or use outside of the             ||
//  ||                     this course.                                           ||
//  ||                                                                            ||
//  ================================================================================

//  ==
//  ||
//  ||  Perform Gauss-Seidel iterations on nrows X nrows linear
//  ||  system:  A*Solution = RHS
//  ||
//  ==

  void GaussSeidel(int max_iter , VD RHS, VD &Solution)
  {
    int converged, it_converged;
    int iter = 0;
    double newval;
    double cur_delta = 0.;
    double max_delta = 0.;
    double tol       = 1.e-04;
    
    converged = 0;
    
    while ( converged == 0 )
      {
	// Check for excessive iterations

	if ( ++iter > max_iter )
	  { 
	    cout << "Gauss-Seidel in " << Name << ": max_iter of " << max_iter << " reached.\n";
	    return;
	  }

	// Initialize flags and values for convergence check
	
	max_delta    = 0.;
	it_converged = 1;

	// Perform one iteration of GS, loop over matrix rows

	rLOOP
	  {
	    // Compute new guess for row r

	    newval = RHS[r];
	    cLOOP if ( c != r ) newval -=  A[r][c] * Solution[c];
	    newval    /= A[r][r];

	    // Convergence check

	    cur_delta  = fabs(Solution[r] - newval);
	    if ( cur_delta > tol ) it_converged = 0;

	    // Record new value in solution

	    Solution[r]       = newval;
	  }

	// Make note of the convergence state
	
	converged = it_converged;
	
      }

    cout << "Gauss-Seidel in " << Name << ": converged in " << iter << " iterations.\n";
  }

