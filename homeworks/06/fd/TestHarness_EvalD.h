
void TestHarness_EvalD(  UserInput &UI )
{
  // Request of UI, the double-precision data intented for testing EvalD (input file)...
  // will be turned as  pointer to a TD object, which we will call here, "myTD"

  TestData *myTD = UI.GetTD("EvalD");

  // Call EvalD with that data

  VD xVals;  xVals.resize( myTD->nD );
  VD yVals;  yVals.resize( myTD->nD );

  // Loop over all provided double-precisions values, calling EvalD for each one,
  // and recording the results in D.
  
  for ( int i  = 1 ; i <= myTD->nD ; ++i )
    {
      xVals[i] = myTD->Ddata[i] ;
      yVals[i] = EvalD( UI , xVals[i] , true );
    }

  // Plot those results in a file that can be used as the STD file in a regression test.

  plot("D_vs_T",xVals,yVals);


}

