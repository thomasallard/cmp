#!/usr/bin/python

import os
    

if __name__ == "__main__":

    try:
        CODE = os.environ['CODE']
    except:
        fatalError('You must set the CODE environment variable to the location of the binary.')

    os.system('./clean')
    os.system(CODE + '/code/fd/fd > fd.tty')
    os.system('grep "Gauss-Seidel in" fd.tty > GS_iter_count.txt')
#   os.system('gnuplot pc')
    os.system('../../../python/testing/compare_01.py -t GS_iter_count.txt -s GS_iter_count.txt_std')

    

    
