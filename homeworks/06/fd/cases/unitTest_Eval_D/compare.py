#!/usr/bin/python

import os
    

if __name__ == "__main__":

    try:
        CODE = os.environ['CODE']
    except:
        fatalError('You must set the CODE environment variable to the location of the binary.')

    os.system('./clean')
    os.system(CODE + '/code/fd/fd > fd.tty')
    os.system('gnuplot pc')
    os.system('../../../python/testing/compare_01.py -t D_vs_T.plt -s D_vs_T.plt_std')

    

    
