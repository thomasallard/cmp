#!/usr/bin/python

import os



# Add routine here that opens fd.tty, reads it, looks for the line
# that has the "L2" error in it.  Parses that line, retreives the
# number of points and the L2 error from that line, and returns that
# value.

def getL2error():

    f = open('fd.tty','r')

    for line in f:
        if "L2" in line:
            tmp = line.split()
            npoints = int ( tmp[1] )
            error   = float( tmp[2] )

            return npoints, error

    f.close();
    

if __name__ == "__main__":

    try:
        CODE = os.environ['CODE']
    except:
        fatalError('You must set the CODE environment variable to the location of the binary.')

    L2_vs_npts = {}
    
    os.chdir('01')
    os.system('../clean')
    os.system(CODE + '/code/fd/fd > fd.tty')
    os.system('cat F_phi_0.plt > ../F_phi_0.plt')
    npoints, error = getL2error();
    L2_vs_npts[npoints] = error
    os.chdir('..')
    
    os.chdir('02')
    os.system('../clean')
    os.system(CODE + '/code/fd/fd > fd.tty')
    os.system('cat F_phi_0.plt >> ../F_phi_0.plt')
    npoints, error = getL2error();
    L2_vs_npts[npoints] = error
    os.chdir('..')
    
    os.chdir('03')
    os.system('../clean')
    os.system(CODE + '/code/fd/fd > fd.tty')
    os.system('cat F_phi_0.plt >> ../F_phi_0.plt')
    npoints, error = getL2error();
    L2_vs_npts[npoints] = error
    os.chdir('..')

    g = open('convergence.plt','w')
    for e in sorted(L2_vs_npts):
        print >> g , e , L2_vs_npts[e]
    g.close()
    
    os.system('gnuplot pc')
    os.system('../../../python/testing/compare_01.py -t F_phi_0.plt -s F_phi_0.plt_std')

